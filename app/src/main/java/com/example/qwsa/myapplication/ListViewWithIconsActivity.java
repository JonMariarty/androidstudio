package com.example.qwsa.myapplication;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class ListViewWithIconsActivity extends ListActivity {

    // Определяем массив типа String

    String[] mSign = { "A", "B", "C", "D","E", "F" };

    /** Called when activity is first created. */





    //old text
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setListAdapter(new MyCustomAdapter(this, R.layout.list_item, mSign));
        //setListAdapter(new ArrayAdapter<String>(this, R.layout.list_item, R.id.letter, letters));

        //setContentView(R.layout.activity_main); //тут хз
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        // TODO Auto-generated method stub
        // super.onListItemClick(l, v, position, id);
        String selection = l.getItemAtPosition(position).toString();
        Toast.makeText(this, selection, Toast.LENGTH_SHORT).show();
    }

    private class MyCustomAdapter extends ArrayAdapter<String> {

            public MyCustomAdapter(Context context, int textViewResourseId, String[] objects) {
                super(context, textViewResourseId, objects);
            // TODO Auto-generated constructor stub
        }

        @Override
        //public View getView(int position, View convertView, ViewGroup parent) {
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            // return super.getView(position, convertView, parent);
            LayoutInflater inflater = getLayoutInflater();
            View row = inflater.inflate(R.layout.list_item, parent, false);
            TextView label = (TextView) row.findViewById(R.id.sign);
            label.setText(mSign[position]);
            ImageView icon = (ImageView) row.findViewById(R.id.icon);
            // Если текст содержит ...., то выводим значок ....
            if (mSign[position] == "A") {
                icon.setImageResource(R.drawable.a_icon); }
            if (mSign[position] == "B") {
                icon.setImageResource(R.drawable.b_icon); }
            if (mSign[position] == "C") {
                icon.setImageResource(R.drawable.c_icon); }
            if (mSign[position] == "D") {
                icon.setImageResource(R.drawable.d_icon); }
            if (mSign[position] == "E") {
                icon.setImageResource(R.drawable.e_icon); }
            if (mSign[position] == "F") {
                icon.setImageResource(R.drawable.f_icon);
            } /**else {
                icon.setImageResource(R.drawable.a_icon);
            } */

            return row;
        }
    }

    //old text
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
