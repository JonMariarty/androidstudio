package com.example.qwsa.hashmap;

import java.util.ArrayList;

import android.app.ListActivity;

import android.os.Bundle;

import android.widget.ListAdapter;
import android.widget.SimpleAdapter;

public class MainActivity extends ListActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayList<Contact> list = new ArrayList<Contact>();
        // Заполняем данными
        list.add(new Contact("Барсик", "1111"));
        list.add(new Contact("Мурзик", "22222"));
        list.add(new Contact("Рыжик", "33333"));
        list.add(new Contact("Кузя", "44444"));
        list.add(new Contact("Пушок", "55555"));
        list.add(new Contact("Васька", "64656"));

        ListAdapter adapter = new SimpleAdapter(this, list, R.layout.list_item,
                new String[] { Contact.NAME, Contact.PHONE }, new int[] {
                R.id.tvName, R.id.tvPhone });
        setListAdapter(adapter);
    }
}